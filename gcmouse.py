#! /usr/bin/python

# A wave in front of the kinect would register the hand to control the co-ordinates of the mouse

# A single click is performed by using the other hand to perform a click gesture

# A double click is performed by using the other hand to perform a wave gesture

# A right click is performed by using the other hand to perform 2 back to back click gestures.

from openni import *
from pymouse import PyMouse

context = Context()
context.init()

# Initiation Segment

m = PyMouse() 
depth_generator = DepthGenerator()
depth_generator.create(context)
depth_generator.set_resolution_preset(RES_VGA)
depth_generator.fps = 30

gesture_generator = GestureGenerator()
gesture_generator.create(context)
gesture_generator.add_gesture('Wave')
gesture_generator.add_gesture("Click")

hands_generator = HandsGenerator()
hands_generator.create(context)

#Global Variable Declaration

noise = 6
prevhand = [0,0,0]
mouseid = 0
clikcount = 0
prevclik = [0,0]

# Declare the callbacks

# gesture

def gesture_detected(src, gesture, id, end_point):
    global clikcount, prevclik
    print "Detected gesture:", gesture
    if(gesture == "Wave" and mouseid==0): hands_generator.start_tracking(end_point)
    if(gesture == "Wave" and mouseid!=0):
        m.click(m.position()[0],m.position()[1],1)
        m.click(m.position()[0],m.position()[1],1)
    else:
        if(clikcount==1):
            if(m.position()[0]>=prevclik[0] - 30 and m.position()[0]<=prevclik[0] + 30 and m.position()[1]>=prevclik[1] - 30 and m.position()[1]<=prevclik[1] + 30):
                m.click(m.position()[0],m.position()[1],2)
            else: 
                m.click(m.position()[0],m.position()[1],1)
            clikcount = 0
        else:
            m.click(m.position()[0],m.position()[1],1)
            prevclik = m.position()
            clikcount = clikcount + 1
# gesture_detected

def gesture_progress(src, gesture, point, progress):
	print progress
# gesture_progress

def create(src, id, pos, time):
    global mouseid
    mouseid = id 
    global prevhand
    print 'Create ', id, pos
    m.move(683,384)
    prevhand = pos

# create

def update(src, id, pos, time):
    global prevhand,noise
    print 'Update ', id, pos
    curpos = m.position()
    dx = prevhand[0] - pos [0]
    dy = -pos[1] + prevhand[1]
    dx = dx * (1366/300)
    dy = dy * (768/200)
    if(dx<=noise and dx>=-noise): 
        dx=0;
    if(dy<=noise and dy>=-noise):
        dy=0;
    m.move(curpos[0]+dx,curpos[1]+dy)
    prevhand = pos

# update

def destroy(src, id, time):
    global mouseid
    mouseid = 0
    print 'Destroy ', id
# destroy

# Register the callbacks

gesture_generator.register_gesture_cb(gesture_detected, gesture_progress)
hands_generator.register_hand_cb(create, update, destroy)

# Start generating

context.start_generating_all()

print 'Make a Wave to lock your hand..!'

while True:
    context.wait_any_update_all()
# while
